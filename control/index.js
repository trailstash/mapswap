import { fmtURL } from "../swap/render.js";
import { pointInPolygon } from "../swap/pointInPolygon.js";
import { h, t } from "../swap/dom.js";

const defaultTagIcons = {
  osm: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTQxNiAyMDhjMCA0NS45LTE0LjkgODguMy00MCAxMjIuN0w1MDIuNiA0NTcuNGMxMi41IDEyLjUgMTIuNSAzMi44IDAgNDUuM3MtMzIuOCAxMi41LTQ1LjMgMEwzMzAuNyAzNzZjLTM0LjQgMjUuMi03Ni44IDQwLTEyMi43IDQwQzkzLjEgNDE2IDAgMzIyLjkgMCAyMDhTOTMuMSAwIDIwOCAwUzQxNiA5My4xIDQxNiAyMDh6TTIwOCAzNTJhMTQ0IDE0NCAwIDEgMCAwLTI4OCAxNDQgMTQ0IDAgMSAwIDAgMjg4eiIvPjwvc3ZnPg==",
  outdoor:
    "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0NDggNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTIxMC42IDUuOUw2MiAxNjkuNGMtMy45IDQuMi02IDkuOC02IDE1LjVDNTYgMTk3LjcgNjYuMyAyMDggNzkuMSAyMDhsMjQuOSAwTDMwLjYgMjgxLjRjLTQuMiA0LjItNi42IDEwLTYuNiAxNkMyNCAzMDkuOSAzNC4xIDMyMCA0Ni42IDMyMEw4MCAzMjAgNS40IDQwOS41QzEuOSA0MTMuNyAwIDQxOSAwIDQyNC41YzAgMTMgMTAuNSAyMy41IDIzLjUgMjMuNUwxOTIgNDQ4bDAgMzJjMCAxNy43IDE0LjMgMzIgMzIgMzJzMzItMTQuMyAzMi0zMmwwLTMyIDE2OC41IDBjMTMgMCAyMy41LTEwLjUgMjMuNS0yMy41YzAtNS41LTEuOS0xMC44LTUuNC0xNUwzNjggMzIwbDMzLjQgMGMxMi41IDAgMjIuNi0xMC4xIDIyLjYtMjIuNmMwLTYtMi40LTExLjgtNi42LTE2TDM0NCAyMDhsMjQuOSAwYzEyLjcgMCAyMy4xLTEwLjMgMjMuMS0yMy4xYzAtNS43LTIuMS0xMS4zLTYtMTUuNUwyMzcuNCA1LjlDMjM0IDIuMSAyMjkuMSAwIDIyNCAwcy0xMCAyLjEtMTMuNCA1Ljl6Ii8+PC9zdmc+",
  bike: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA2NDAgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTMxMiAzMmMtMTMuMyAwLTI0IDEwLjctMjQgMjRzMTAuNyAyNCAyNCAyNGwyNS43IDAgMzQuNiA2NC0xNDkuNCAwLTI3LjQtMzhDMTkxIDk5LjcgMTgzLjcgOTYgMTc2IDk2bC01NiAwYy0xMy4zIDAtMjQgMTAuNy0yNCAyNHMxMC43IDI0IDI0IDI0bDQzLjcgMCAyMi4xIDMwLjctMjYuNiA1My4xYy0xMC0yLjUtMjAuNS0zLjgtMzEuMi0zLjhDNTcuMyAyMjQgMCAyODEuMyAwIDM1MnM1Ny4zIDEyOCAxMjggMTI4YzY1LjMgMCAxMTkuMS00OC45IDEyNy0xMTJsNDkgMGM4LjUgMCAxNi4zLTQuNSAyMC43LTExLjhsODQuOC0xNDMuNSAyMS43IDQwLjFDNDAyLjQgMjc2LjMgMzg0IDMxMiAzODQgMzUyYzAgNzAuNyA1Ny4zIDEyOCAxMjggMTI4czEyOC01Ny4zIDEyOC0xMjhzLTU3LjMtMTI4LTEyOC0xMjhjLTEzLjUgMC0yNi41IDIuMS0zOC43IDZMMzc1LjQgNDguOEMzNjkuOCAzOC40IDM1OSAzMiAzNDcuMiAzMkwzMTIgMzJ6TTQ1OC42IDMwMy43bDMyLjMgNTkuN2M2LjMgMTEuNyAyMC45IDE2IDMyLjUgOS43czE2LTIwLjkgOS43LTMyLjVsLTMyLjMtNTkuN2MzLjYtLjYgNy40LS45IDExLjItLjljMzkuOCAwIDcyIDMyLjIgNzIgNzJzLTMyLjIgNzItNzIgNzJzLTcyLTMyLjItNzItNzJjMC0xOC42IDctMzUuNSAxOC42LTQ4LjN6TTEzMy4yIDM2OGw2NSAwYy03LjMgMzIuMS0zNiA1Ni03MC4yIDU2Yy0zOS44IDAtNzItMzIuMi03Mi03MnMzMi4yLTcyIDcyLTcyYzEuNyAwIDMuNCAuMSA1LjEgLjJsLTI0LjIgNDguNWMtOSAxOC4xIDQuMSAzOS40IDI0LjMgMzkuNHptMzMuNy00OGw1MC43LTEwMS4zIDcyLjkgMTAxLjItLjEgLjEtMTIzLjUgMHptOTAuNi0xMjhsMTA4LjUgMEwzMTcgMjc0LjggMjU3LjQgMTkyeiIvPjwvc3ZnPg==",
  govt: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTI0MC4xIDQuMmM5LjgtNS42IDIxLjktNS42IDMxLjggMGwxNzEuOCA5OC4xTDQ0OCAxMDRsMCAuOSA0Ny45IDI3LjRjMTIuNiA3LjIgMTguOCAyMiAxNS4xIDM2cy0xNi40IDIzLjgtMzAuOSAyMy44TDMyIDE5MmMtMTQuNSAwLTI3LjItOS44LTMwLjktMjMuOHMyLjUtMjguOCAxNS4xLTM2TDY0IDEwNC45bDAtLjkgNC40LTEuNkwyNDAuMSA0LjJ6TTY0IDIyNGw2NCAwIDAgMTkyIDQwIDAgMC0xOTIgNjQgMCAwIDE5MiA0OCAwIDAtMTkyIDY0IDAgMCAxOTIgNDAgMCAwLTE5MiA2NCAwIDAgMTk2LjNjLjYgLjMgMS4yIC43IDEuOCAxLjFsNDggMzJjMTEuNyA3LjggMTcgMjIuNCAxMi45IDM1LjlTNDk0LjEgNTEyIDQ4MCA1MTJMMzIgNTEyYy0xNC4xIDAtMjYuNS05LjItMzAuNi0yMi43czEuMS0yOC4xIDEyLjktMzUuOWw0OC0zMmMuNi0uNCAxLjItLjcgMS44LTEuMUw2NCAyMjR6Ii8+PC9zdmc+",
  commercial:
    "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAzMjAgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTE2MCAwYzE3LjcgMCAzMiAxNC4zIDMyIDMybDAgMzUuN2MxLjYgLjIgMy4xIC40IDQuNyAuN2MuNCAuMSAuNyAuMSAxLjEgLjJsNDggOC44YzE3LjQgMy4yIDI4LjkgMTkuOSAyNS43IDM3LjJzLTE5LjkgMjguOS0zNy4yIDI1LjdsLTQ3LjUtOC43Yy0zMS4zLTQuNi01OC45LTEuNS03OC4zIDYuMnMtMjcuMiAxOC4zLTI5IDI4LjFjLTIgMTAuNy0uNSAxNi43IDEuMiAyMC40YzEuOCAzLjkgNS41IDguMyAxMi44IDEzLjJjMTYuMyAxMC43IDQxLjMgMTcuNyA3My43IDI2LjNsMi45IC44YzI4LjYgNy42IDYzLjYgMTYuOCA4OS42IDMzLjhjMTQuMiA5LjMgMjcuNiAyMS45IDM1LjkgMzkuNWM4LjUgMTcuOSAxMC4zIDM3LjkgNi40IDU5LjJjLTYuOSAzOC0zMy4xIDYzLjQtNjUuNiA3Ni43Yy0xMy43IDUuNi0yOC42IDkuMi00NC40IDExbDAgMzMuNGMwIDE3LjctMTQuMyAzMi0zMiAzMnMtMzItMTQuMy0zMi0zMmwwLTM0LjljLS40LS4xLS45LS4xLTEuMy0uMmwtLjIgMHMwIDAgMCAwYy0yNC40LTMuOC02NC41LTE0LjMtOTEuNS0yNi4zYy0xNi4xLTcuMi0yMy40LTI2LjEtMTYuMi00Mi4yczI2LjEtMjMuNCA0Mi4yLTE2LjJjMjAuOSA5LjMgNTUuMyAxOC41IDc1LjIgMjEuNmMzMS45IDQuNyA1OC4yIDIgNzYtNS4zYzE2LjktNi45IDI0LjYtMTYuOSAyNi44LTI4LjljMS45LTEwLjYgLjQtMTYuNy0xLjMtMjAuNGMtMS45LTQtNS42LTguNC0xMy0xMy4zYy0xNi40LTEwLjctNDEuNS0xNy43LTc0LTI2LjNsLTIuOC0uN3MwIDAgMCAwQzExOS40IDI3OS4zIDg0LjQgMjcwIDU4LjQgMjUzYy0xNC4yLTkuMy0yNy41LTIyLTM1LjgtMzkuNmMtOC40LTE3LjktMTAuMS0zNy45LTYuMS01OS4yQzIzLjcgMTE2IDUyLjMgOTEuMiA4NC44IDc4LjNjMTMuMy01LjMgMjcuOS04LjkgNDMuMi0xMUwxMjggMzJjMC0xNy43IDE0LjMtMzIgMzItMzJ6Ii8+PC9zdmc+",
  routing:
    "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTUxMiA5NmMwIDUwLjItNTkuMSAxMjUuMS04NC42IDE1NWMtMy44IDQuNC05LjQgNi4xLTE0LjUgNUwzMjAgMjU2Yy0xNy43IDAtMzIgMTQuMy0zMiAzMnMxNC4zIDMyIDMyIDMybDk2IDBjNTMgMCA5NiA0MyA5NiA5NnMtNDMgOTYtOTYgOTZsLTI3Ni40IDBjOC43LTkuOSAxOS4zLTIyLjYgMzAtMzYuOGM2LjMtOC40IDEyLjgtMTcuNiAxOS0yNy4yTDQxNiA0NDhjMTcuNyAwIDMyLTE0LjMgMzItMzJzLTE0LjMtMzItMzItMzJsLTk2IDBjLTUzIDAtOTYtNDMtOTYtOTZzNDMtOTYgOTYtOTZsMzkuOCAwYy0yMS0zMS41LTM5LjgtNjcuNy0zOS44LTk2YzAtNTMgNDMtOTYgOTYtOTZzOTYgNDMgOTYgOTZ6TTExNy4xIDQ4OS4xYy0zLjggNC4zLTcuMiA4LjEtMTAuMSAxMS4zbC0xLjggMi0uMi0uMmMtNiA0LjYtMTQuNiA0LTIwLTEuOEM1OS44IDQ3MyAwIDQwMi41IDAgMzUyYzAtNTMgNDMtOTYgOTYtOTZzOTYgNDMgOTYgOTZjMCAzMC0yMS4xIDY3LTQzLjUgOTcuOWMtMTAuNyAxNC43LTIxLjcgMjgtMzAuOCAzOC41bC0uNiAuN3pNMTI4IDM1MmEzMiAzMiAwIDEgMCAtNjQgMCAzMiAzMiAwIDEgMCA2NCAwek00MTYgMTI4YTMyIDMyIDAgMSAwIDAtNjQgMzIgMzIgMCAxIDAgMCA2NHoiLz48L3N2Zz4=",
  editor:
    "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTQ3MS42IDIxLjdjLTIxLjktMjEuOS01Ny4zLTIxLjktNzkuMiAwTDM2Mi4zIDUxLjdsOTcuOSA5Ny45IDMwLjEtMzAuMWMyMS45LTIxLjkgMjEuOS01Ny4zIDAtNzkuMkw0NzEuNiAyMS43em0tMjk5LjIgMjIwYy02LjEgNi4xLTEwLjggMTMuNi0xMy41IDIxLjlsLTI5LjYgODguOGMtMi45IDguNi0uNiAxOC4xIDUuOCAyNC42czE1LjkgOC43IDI0LjYgNS44bDg4LjgtMjkuNmM4LjItMi43IDE1LjctNy40IDIxLjktMTMuNUw0MzcuNyAxNzIuMyAzMzkuNyA3NC4zIDE3Mi40IDI0MS43ek05NiA2NEM0MyA2NCAwIDEwNyAwIDE2MEwwIDQxNmMwIDUzIDQzIDk2IDk2IDk2bDI1NiAwYzUzIDAgOTYtNDMgOTYtOTZsMC05NmMwLTE3LjctMTQuMy0zMi0zMi0zMnMtMzIgMTQuMy0zMiAzMmwwIDk2YzAgMTcuNy0xNC4zIDMyLTMyIDMyTDk2IDQ0OGMtMTcuNyAwLTMyLTE0LjMtMzItMzJsMC0yNTZjMC0xNy43IDE0LjMtMzIgMzItMzJsOTYgMGMxNy43IDAgMzItMTQuMyAzMi0zMnMtMTQuMy0zMi0zMi0zMkw5NiA2NHoiLz48L3N2Zz4=",
  tool: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTM1MiAzMjBjODguNCAwIDE2MC03MS42IDE2MC0xNjBjMC0xNS4zLTIuMi0zMC4xLTYuMi00NC4yYy0zLjEtMTAuOC0xNi40LTEzLjItMjQuMy01LjNsLTc2LjggNzYuOGMtMyAzLTcuMSA0LjctMTEuMyA0LjdMMzM2IDE5MmMtOC44IDAtMTYtNy4yLTE2LTE2bDAtNTcuNGMwLTQuMiAxLjctOC4zIDQuNy0xMS4zbDc2LjgtNzYuOGM3LjktNy45IDUuNC0yMS4yLTUuMy0yNC4zQzM4Mi4xIDIuMiAzNjcuMyAwIDM1MiAwQzI2My42IDAgMTkyIDcxLjYgMTkyIDE2MGMwIDE5LjEgMy40IDM3LjUgOS41IDU0LjVMMTkuOSAzOTYuMUM3LjIgNDA4LjggMCA0MjYuMSAwIDQ0NC4xQzAgNDgxLjYgMzAuNCA1MTIgNjcuOSA1MTJjMTggMCAzNS4zLTcuMiA0OC0xOS45TDI5Ny41IDMxMC41YzE3IDYuMiAzNS40IDkuNSA1NC41IDkuNXpNODAgNDA4YTI0IDI0IDAgMSAxIDAgNDggMjQgMjQgMCAxIDEgMC00OHoiLz48L3N2Zz4=",
  ski: "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTM4MC43IDQ4YTQ4IDQ4IDAgMSAxIDk2IDAgNDggNDggMCAxIDEgLTk2IDB6TTIuNyAyNjguOWM2LjEtMTEuOCAyMC42LTE2LjMgMzIuNC0xMC4yTDIzMi43IDM2MS4zbDQ2LjItNjkuMi03NS4xLTc1LjFjLTE0LjYtMTQuNi0yMC40LTMzLjktMTguNC01Mi4xbDEwOC44IDUyIDM5LjMgMzkuM2MxNi4yIDE2LjIgMTguNyA0MS41IDYgNjAuNkwyODkuOCAzOTFsMTI4LjcgNjYuOGMxMy42IDcuMSAyOS44IDcuMiA0My42IC4zbDE1LjItNy42YzExLjktNS45IDI2LjMtMS4xIDMyLjIgMTAuN3MxLjEgMjYuMy0xMC43IDMyLjJsLTE1LjIgNy42Yy0yNy41IDEzLjctNTkuOSAxMy41LTg3LjItLjdMMTIuOSAzMDEuM0MxLjIgMjk1LjItMy40IDI4MC43IDIuNyAyNjguOXpNMTE4LjkgNjUuNkwxMzcgNzQuMmw4LjctMTcuNGM0LTcuOSAxMy42LTExLjEgMjEuNS03LjJzMTEuMSAxMy42IDcuMiAyMS41bC04LjUgMTYuOSA1NC43IDI2LjJjMS41LS43IDMuMS0xLjQgNC43LTIuMWw4My40LTMzLjRjMzQuMi0xMy43IDcyLjggNC4yIDg0LjUgMzkuMmwxNy4xIDUxLjIgNTIuMSAyNi4xYzE1LjggNy45IDIyLjIgMjcuMSAxNC4zIDQyLjlzLTI3LjEgMjIuMi00Mi45IDE0LjNsLTU4LjEtMjljLTExLjQtNS43LTIwLTE1LjctMjQuMS0yNy44bC01LjgtMTcuMy0yNy4zIDEyLjEtNi44IDMtNi43LTMuMkwxNTEuNSAxMTYuN2wtOS4yIDE4LjRjLTQgNy45LTEzLjYgMTEuMS0yMS41IDcuMnMtMTEuMS0xMy42LTcuMi0yMS41bDktMTgtMTcuNi04LjRjLTgtMy44LTExLjMtMTMuNC03LjUtMjEuM3MxMy40LTExLjMgMjEuMy03LjV6Ii8+PC9zdmc+",
  parcel:
    "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0NDggNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTM4NCA5NmwwIDEyOC0xMjggMCAwLTEyOCAxMjggMHptMCAxOTJsMCAxMjgtMTI4IDAgMC0xMjggMTI4IDB6TTE5MiAyMjRMNjQgMjI0IDY0IDk2bDEyOCAwIDAgMTI4ek02NCAyODhsMTI4IDAgMCAxMjhMNjQgNDE2bDAtMTI4ek02NCAzMkMyOC43IDMyIDAgNjAuNyAwIDk2TDAgNDE2YzAgMzUuMyAyOC43IDY0IDY0IDY0bDMyMCAwYzM1LjMgMCA2NC0yOC43IDY0LTY0bDAtMzIwYzAtMzUuMy0yOC43LTY0LTY0LTY0TDY0IDMyeiIvPjwvc3ZnPg==",
};
const LOGO_URL = new URL("../logo.svg", import.meta.url).toString();
const LINK_SVG_DATA_URL =
  "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTMyMCAwYy0xNy43IDAtMzIgMTQuMy0zMiAzMnMxNC4zIDMyIDMyIDMybDgyLjcgMEwyMDEuNCAyNjUuNGMtMTIuNSAxMi41LTEyLjUgMzIuOCAwIDQ1LjNzMzIuOCAxMi41IDQ1LjMgMEw0NDggMTA5LjNsMCA4Mi43YzAgMTcuNyAxNC4zIDMyIDMyIDMyczMyLTE0LjMgMzItMzJsMC0xNjBjMC0xNy43LTE0LjMtMzItMzItMzJMMzIwIDB6TTgwIDMyQzM1LjggMzIgMCA2Ny44IDAgMTEyTDAgNDMyYzAgNDQuMiAzNS44IDgwIDgwIDgwbDMyMCAwYzQ0LjIgMCA4MC0zNS44IDgwLTgwbDAtMTEyYzAtMTcuNy0xNC4zLTMyLTMyLTMycy0zMiAxNC4zLTMyIDMybDAgMTEyYzAgOC44LTcuMiAxNi0xNiAxNkw4MCA0NDhjLTguOCAwLTE2LTcuMi0xNi0xNmwwLTMyMGMwLTguOCA3LjItMTYgMTYtMTZsMTEyIDBjMTcuNyAwIDMyLTE0LjMgMzItMzJzLTE0LjMtMzItMzItMzJMODAgMzJ6Ii8+PC9zdmc+";
const MAP_SVG_DATA_URL =
  "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1NzYgNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTU2NS42IDM2LjJDNTcyLjEgNDAuNyA1NzYgNDguMSA1NzYgNTZsMCAzMzZjMCAxMC02LjIgMTguOS0xNS41IDIyLjRsLTE2OCA2NGMtNS4yIDItMTAuOSAyLjEtMTYuMSAuM0wxOTIuNSA0MTcuNWwtMTYwIDYxYy03LjQgMi44LTE1LjcgMS44LTIyLjItMi43UzAgNDYzLjkgMCA0NTZMMCAxMjBjMC0xMCA2LjEtMTguOSAxNS41LTIyLjRsMTY4LTY0YzUuMi0yIDEwLjktMi4xIDE2LjEtLjNMMzgzLjUgOTQuNWwxNjAtNjFjNy40LTIuOCAxNS43LTEuOCAyMi4yIDIuN3pNNDggMTM2LjVsMCAyODQuNiAxMjAtNDUuNyAwLTI4NC42TDQ4IDEzNi41ek0zNjAgNDIyLjdsMC0yODUuNC0xNDQtNDggMCAyODUuNCAxNDQgNDh6bTQ4LTEuNWwxMjAtNDUuNyAwLTI4NC42TDQwOCAxMzYuNWwwIDI4NC42eiIvPjwvc3ZnPg==";
const TAG_SVG_DATA_URL =
  "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0NDggNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTAgODBMMCAyMjkuNWMwIDE3IDYuNyAzMy4zIDE4LjcgNDUuM2wxNzYgMTc2YzI1IDI1IDY1LjUgMjUgOTAuNSAwTDQxOC43IDMxNy4zYzI1LTI1IDI1LTY1LjUgMC05MC41bC0xNzYtMTc2Yy0xMi0xMi0yOC4zLTE4LjctNDUuMy0xOC43TDQ4IDMyQzIxLjUgMzIgMCA1My41IDAgODB6bTExMiAzMmEzMiAzMiAwIDEgMSAwIDY0IDMyIDMyIDAgMSAxIDAtNjR6Ii8+PC9zdmc+";
const CARET_SVG_DATA_URL =
  "data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA0NDggNTEyIj48IS0tIUZvbnQgQXdlc29tZSBGcmVlIDYuNi4wIGJ5IEBmb250YXdlc29tZSAtIGh0dHBzOi8vZm9udGF3ZXNvbWUuY29tIExpY2Vuc2UgLSBodHRwczovL2ZvbnRhd2Vzb21lLmNvbS9saWNlbnNlL2ZyZWUgQ29weXJpZ2h0IDIwMjQgRm9udGljb25zLCBJbmMuLS0+PHBhdGggZD0iTTIwMS40IDEzNy40YzEyLjUtMTIuNSAzMi44LTEyLjUgNDUuMyAwbDE2MCAxNjBjMTIuNSAxMi41IDEyLjUgMzIuOCAwIDQ1LjNzLTMyLjggMTIuNS00NS4zIDBMMjI0IDIwNS4zIDg2LjYgMzQyLjZjLTEyLjUgMTIuNS0zMi44IDEyLjUtNDUuMyAwcy0xMi41LTMyLjggMC00NS4zbDE2MC0xNjB6Ii8+PC9zdmc+";
let i = 0;
const CSS = `
.maplibre-mapswap img {
  width: 18px;
  height: 18px;
  object-fit: cover;
}
.mapswap-control-visibility:checked ~ .mapswap-control { display: block; }
.mapswap-control {
  background: rgba(255,255,255,0.5);
  backdrop-filter: blur(5px);
  -webkit-backdrop-filter: blur(5px);

  display: none;
  position: fixed;
  min-height: 100px;
  min-width: 100px;
  width: 400px;
  max-height: calc(100% - 25px);
  overflow-y: auto;
  border-radius: 12px;
  text-align: left;
  padding-bottom: 5px;
  box-shadow: 0 0 2px black;
  z-index: 10; /* ensure it's above the attribution control */

  /* mimic .maplibre-ctrl */
  clear: both;
  pointer-events: auto;
  transform: translate(0);
}
@media (max-width: 900px) {
  .mapswap-control {
    width: calc(100% - 20px);
    max-height: 80%;
  }
}
.maplibregl-ctrl-bottom-left .mapswap-control {
  bottom: 10px;
  left: 10px;
}
.maplibregl-ctrl-top-left .mapswap-control {
  top: 10px;
  left: 10px;
}
.maplibregl-ctrl-bottom-right .mapswap-control {
  bottom: 10px;
  right: 10px;
}
.maplibregl-ctrl-top-right .mapswap-control {
  top: 10px;
  right: 10px;
}
.mapswap-control .map {
  display: block;
  padding: 5px;
  padding-bottom: 0;
  cursor: pointer;
}
.mapswap-control .map > a {
  color: black;
  font-size: 1rem;
  text-decoration: none;
  text-align: left;
  background: rgba(0,0,0,0.05);
  padding: 5px;
  display: block;
  border-radius: 10px;
  overflow-x: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}
.mapswap-control .map > a:hover{
  box-shadow: inset 0 0 0 99em rgba(255,255,255,.1);
  backdrop-filter: blur(10px);
  -webkit-backdrop-filter: blur(10px);
}
.mapswap-control input[type=radio] {
  float: right;
  margin: 12.5px;
}
.mapswap-control .icon {
  width: 32px;
  height: 32px;
  object-fit: contain;
  vertical-align: middle;
  background: rgba(255,255,255,0.4);
  padding: 5px;
  border-radius: 10px;
  float: left;
  margin-right: 10px
}
.map small {
  color: #777;
  font-weight: 300;
  font-size: 0.8em;
}
.mapswap-control .link {
  width: 16px;
  margin: 10.5px 10px;
  vertical-align: middle;
  float: right;
}
.mapswap-control .caret {
  width: 16px;
  margin: 10.5px 10px;
  vertical-align: middle;
  float: right;
  transform: rotate(180deg);
}
.mapswap-control .open .caret {
  transform: rotate(0deg);
}
.mapswap-control .tag-maps {
  display: none;
}
.mapswap-control .open .tag-maps {
  display: block;
}
.mapswap-control .map.style .link:hover {
  width: 16px;
  margin: 10.5px -3px;
}
.mapswap-control .map.style .link {
  float: right;
  width: 10px;
  margin: 13.5px 0;
}
.mapswap-control-visibility { display: none }
.mapswap-control footer {
  margin-right: 10px;
  margin-top: 5px;
  text-align: center;
}
.mapswap-control footer a {
  text-decoration: none;
  color: black;
  font-weight: bold;
}
.mapswap-control h1 {
  text-align: left;
  font-size: 16px;
  font-weight: bold;
  margin: 10px
}
.mapswap-control h1 img {
  vertical-align: middle;
  height: 26px;
  width: 26px;
}
.mapswap-control .close {
  float: right;
  margin: 0 4px;
  font-size: 20px;
  cursor: pointer;
}
`;

export default class MapSwapControl {
  #map;
  #openTags;
  constructor(
    maps = new URL("../swap/maps.json", import.meta.url).toString(),

    {
      title = "MapSwap",
      icon = LOGO_URL,
      attribution = false,
      header = true,
      tagIcons = defaultTagIcons,
    } = {},
  ) {
    this.maps = maps;
    this.close = this.close.bind(this);
    this.render = this.render.bind(this);
    this.attribution = attribution;
    this.icon = icon;
    this.title = title;
    this.header = header;
    this.tagIcons = tagIcons;
    this.#openTags = new Set();
  }
  onAdd(map) {
    this.#map = map;
    this._container = h("div");
    this._container.addEventListener("contextmenu", (e) => e.preventDefault());
    i++;
    this._container.innerHTML = `
      <style>
        ${CSS}
      </style>
      <input type="checkbox" id="mapswap-control-visibility-${i}" class="mapswap-control-visibility"></input>
      <div class="maplibregl-ctrl maplibregl-ctrl-group">
        <button>
          <label for="mapswap-control-visibility-${i}">
            <span style="display:flex;align-items:center;justify-content:center;" class="maplibregl-ctrl-icon maplibre-mapswap" aria-hidden="true" title="${this.title}">
              <img src="${this.icon}"}></img>
            </span>
          </label>

        </button>
      </div>
      <div class="mapswap-control"></div>`;

    this._container
      .querySelector("label")
      .addEventListener("click", this.render);
    this.#map.on("moveend", this.render);

    return this._container;
  }
  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.#map = undefined;
  }
  getDefaultPosition() {
    return "bottom-right";
  }
  close() {
    if (this._container?.querySelector(".mapswap-control-visibility:checked")) {
      this._container
        ?.querySelector(".mapswap-control-visibility:checked")
        .click();
    }
  }
  async render(currentStyle) {
    if (typeof this.maps === "string") {
      this._container
        .querySelector(".mapswap-control")
        .replaceWith(
          h(
            "div",
            { className: "mapswap-control" },
            h(
              "h1",
              {},
              h("img", { src: this.icon }),
              t(" " + this.title),
              h("span", { className: "close", onclick: this.close }, t("×")),
            ),
            h("h1", {}, h("small", {}, t("Loading..."))),
          ),
        );
      const resp = await fetch(this.maps);
      if (!resp.ok) {
        throw new Error("error fetching mapsA;");
      }
      this.maps = await resp.json();
    }
    if (typeof currentStyle !== "string") {
      currentStyle = this.#map.getStyle().name;
    }
    const center = this.#map.getCenter().toArray();
    const zoom = this.#map.getZoom();
    const xyz = { z: zoom, x: center[0], y: center[1] };

    const Tags = new Set();
    for (const i in this.maps) {
      let tags = this.maps[i].tags;
      if (!tags) {
        tags = ["default"];
        this.maps[i].tags = tags;
      }
      for (const tag of tags || []) {
        Tags.add(tag);
      }
    }

    const renderMap = (
      { template, name, icon, tags, styleName, style, description },
      i,
    ) =>
      h(
        "div",
        {
          className: `map ${template ? "template" : ""} ${
            style ? "style" : ""
          }`,
        },
        h(
          "a",
          style
            ? {
                onclick: (e) => {
                  e.stopPropagation();
                  if (!e.target.classList.contains("link")) {
                    this.#map.setStyle(style);
                    this.render(styleName || name);
                  }
                },
              }
            : {
                target: "_blank",
                href: fmtURL(template, xyz),
              },
          h("img", {
            className: "icon",
            src: icon || MAP_SVG_DATA_URL,
          }),
          // name
          currentStyle === (styleName || name)
            ? h("b", {}, t(" " + name))
            : t(" " + name),
          // style selection indicator
          !style
            ? t("")
            : h("input", {
                type: "radio",
                checked: currentStyle === (styleName || name),
              }),
          // external link indicator
          template
            ? h(
                "a",
                { target: "_blank", href: fmtURL(template, xyz) },
                h("img", {
                  className: `link`,
                  src: LINK_SVG_DATA_URL,
                }),
              )
            : t(""),
          // tags
          h("br"),
          h(
            "small",
            { className: "tags" },
            description
              ? t(description)
              : tags
                ? t("tags: " + tags.join(", "))
                : t(""),
          ),
        ),
      );
    const renderTag = (tag) => {
      const tagMaps = maps.filter(({ tags }) => tags.includes(tag));
      if (!tagMaps.length) {
        return h("span");
      }
      return h(
        "div",
        { className: "map" },
        h(
          "a",
          {
            className: this.#openTags.has(tag) ? "open" : "",
            onclick: (e) => {
              e.stopPropagation();
              e.preventDefault();
              e.target.classList.toggle("open");
              if (this.#openTags.has(tag)) {
                this.#openTags.delete(tag);
              } else {
                this.#openTags.add(tag);
              }
            },
          },
          h("img", {
            className: "icon",
            src: this.tagIcons[tag] || TAG_SVG_DATA_URL,
          }),
          t(tag),
          h("img", {
            className: `caret`,
            src: CARET_SVG_DATA_URL,
          }),
          h("br"),
          h("small", { className: "tags" }, t(`${tagMaps.length} maps`)),
          h(
            "div",
            { className: "tag-maps", onclick: (e) => e.stopPropagation() },
            ...tagMaps.map(renderMap),
          ),
        ),
      );
    };
    const maps = this.maps
      .filter(({ name }) => name)
      .filter((m) =>
        m.polygon
          ? m.polygon.some((poly) => pointInPolygon([xyz.x, xyz.y], poly))
          : true,
      );
    this._container.querySelector(".mapswap-control").replaceWith(
      h(
        "div",
        { className: "mapswap-control" },
        h(
          "h1",
          {},
          h("img", { src: this.icon }),
          t(" " + this.title),
          h("span", { className: "close", onclick: this.close }, t("×")),
        ),
        ...maps
          .filter(({ tags }) => Tags.size === 0 || tags.includes("default"))
          .map(renderMap),
        ...Array.from(Tags)
          .filter(
            (tag) =>
              tag !== "default" &&
              !maps.every((x) => x.tags?.includes("default")),
          )
          .map(renderTag),
        this.attribution
          ? h(
              "footer",
              {},
              t("Powered by "),
              h(
                "a",
                { href: "https://mapswap.trailsta.sh/", target: "_blank" },
                h("img", {
                  src: "https://mapswap.trailsta.sh/logo.svg",
                  style: "height: 16px;vertical-align: middle;",
                }),
                t("MapSwap"),
              ),
            )
          : t(""),
      ),
    );
  }
}
