import { URLXYZParser } from "./parse.js";
import { fmtURL, renderNoUrl, renderResult } from "./render.js";
import { pointInPolygon } from "./pointInPolygon.js";

async function init() {
  let resp = await fetch("./maps.json");
  let maps = [];
  if (resp.ok) {
    maps = await resp.json();
  }
  let allMaps = maps;
  const root = window.location.origin + window.location.pathname;
  const searchParams = new URL(window.location).searchParams;
  const hashParams = new URLSearchParams(window.location.hash.slice(1));
  const params = new URLSearchParams({
    ...Object.fromEntries(searchParams),
    ...Object.fromEntries(hashParams),
  });
  const url = params.get("url") || params.get("text");
  if (params.get("maps")) {
    resp = await fetch(params.get("maps"));
    maps = await resp.json();
    allMaps = allMaps.concat(maps);
    if (url) {
      const newParams = new URLSearchParams(params);
      newParams.delete("maps");
      newParams.delete("min");
      document.querySelector("a.title").href = ".#" + newParams.toString();
    }
  }
  if (url) {
    const xyz = new URLXYZParser(allMaps).parse(url, params.get("type"));
    if (maps.length === 1) {
      window.location = fmtURL(maps[0].template, xyz);
    } else {
      maps = maps.filter((m) =>
        m.polygon && xyz
          ? m.polygon.some((poly) => pointInPolygon([xyz.x, xyz.y], poly))
          : true,
      );
      renderResult(maps, xyz, url, params.get("min"), params.get("tag"));
    }
  } else {
    renderNoUrl();
  }
}

init();
