import { h, t } from "./dom.js";
import { bounds } from "./geo-viewport.js";

const defaultFavorites = [
  "OpenStreetMap",
  "Google Maps",
  "Edit OSM with iD",
  "OpenTrailStash",
  "OSM Americana",
];
let favorites = new Set();
try {
  favorites = new Set(
    JSON.parse(
      localStorage.getItem("mapswap-favorites") ||
        JSON.stringify(defaultFavorites),
    ),
  );
} catch {}

export const fmtURL = (tmpl, { z, x, y }) => {
  const [xMin, yMin, xMax, yMax] = bounds([x, y], z + 1, [
    window.innerWidth,
    window.innerHeight,
  ]);
  return tmpl
    .replaceAll("{z}", z)
    .replaceAll("{Z}", z + 1)
    .replaceAll("{zr}", Math.round(z))
    .replaceAll("{Zr}", Math.round(z) + 1)
    .replaceAll("{x}", x)
    .replaceAll("{y}", y)
    .replaceAll("{xMin}", xMin)
    .replaceAll("{xMax}", xMax)
    .replaceAll("{yMin}", yMin)
    .replaceAll("{yMax}", yMax)
    .replaceAll("{xDelta}", xMax - xMin)
    .replaceAll("{yDelta}", yMax - yMin);
};

const getCurrentPosition = (options) =>
  new Promise((resolve, reject) =>
    navigator.geolocation.getCurrentPosition(resolve, reject, options),
  );
const useCurrentLocation = () =>
  h(
    "p",
    {},
    h(
      "a",
      {
        href: "#",
        onclick: async () => {
          const {
            coords: { accuracy, latitude, longitude },
          } = await getCurrentPosition({
            enableHighAccuracy: true,
            maximumAge: 0,
          });
          const screenSize = Math.min(window.innerWidth, window.innerHeight);
          // Equators length
          const equator = 40075004;
          const requiredMpp = accuracy / screenSize;
          const zoom =
            Math.log(equator / (256 * requiredMpp)) / Math.log(2) - 6;

          //var zoom = Math.min(
          //  20,
          //  Math.max(1, Math.log2(591657550 / accuracy) - 2)
          //);
          window.location.hash = `#url=geo:${latitude},${longitude};z=${zoom}`;
          window.location.reload();
        },
      },
      t("Use current location"),
    ),
  );

export const renderNoUrl = () => {
  document
    .getElementById("error")
    .replaceWith(
      h(
        "div",
        { id: "error" },
        h("p", {}, t("No map view detected in URL")),
        useCurrentLocation(),
      ),
    );
};
export const renderResult = (maps, xyz, url, min, currentTag) => {
  if (!xyz) {
    document
      .getElementById("error")
      .replaceWith(
        h(
          "div",
          { id: "error" },
          h("p", {}, t("No map view detected in URL")),
          h("p", {}, h("small", {}, h("a", { href: url }, t(url)))),
          useCurrentLocation(),
        ),
      );
    return;
  }
  if (!document.querySelector(".maplibregl-map")) {
    const map = new maplibregl.Map({
      container: "map-backdrop",
      style: "./style.json",
      center: [xyz.x, xyz.y],
      zoom: xyz.z,
      interactive: false,
      attributionControl: { compact: false },
    });
    const blur = document.createElement("div");
    blur.className = "map-blur";
    document.querySelector(".maplibregl-control-container").appendChild(blur);
    map.on("load", () => blur.classList.add("loaded"));
  }
  const Tags = new Set(["favorites"]);
  for (const { tags } of maps) {
    for (const tag of tags || []) {
      Tags.add(tag);
    }
  }
  if (Tags.size === 1) {
    Tags.clear();
  }
  if (!currentTag) {
    currentTag = Array.from(Tags)[0];
  }
  if (min == null) {
    document
      .getElementById("coords")
      .replaceWith(
        h(
          "div",
          { id: "coords" },
          h(
            "p",
            {},
            t(
              `parsed Zoom(Z)=${xyz.z} / Latitude(Y)=${xyz.y} / Longitude(X)=${xyz.x} from`,
            ),
          ),
          h("p", {}, h("small", {}, h("a", { href: url }, t(url)))),
        ),
      );
  }
  if (!xyz.z) {
    xyz.z = 14;
  }
  document.getElementById("tags").replaceWith(
    h(
      "div",
      { id: "tags" },
      ...Array.from(Tags).map((tag, i) =>
        h(
          "label",
          { for: tag, onclick: () => renderResult(maps, xyz, url, min, tag) },
          h("input", {
            name: "tag",
            value: tag,
            type: "radio",
            checked: tag === currentTag,
          }),
          h("span", {}, t(tag)),
        ),
      ),
    ),
  );
  document.getElementById("maps").replaceWith(
    h(
      "div",
      {
        id: "maps",
        className: "flex one two-700 three-1400 four-2100 center",
      },
      ...maps
        .filter(({ name }) => name)
        .filter(({ name, tags }) => {
          if (currentTag === "favorites") {
            return favorites.has(name);
          } else {
            if (currentTag && tags) {
              return tags.includes(currentTag);
            } else {
              return true;
            }
          }
        })
        .map(({ template, name, icon, tags }, i) =>
          h(
            "div",
            { className: "map" },
            h(
              "a",
              { href: fmtURL(template, xyz), className: "button" },
              h("img", {
                className: "icon",
                src:
                  icon ||
                  "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==",
              }),
              t(" " + name),
              Tags.size
                ? h(
                    "span",
                    {
                      className: `favorite ${favorites.has(name) ? "fav" : ""}`,
                      onclick: (e) => {
                        e.preventDefault();
                        if (favorites.has(name)) {
                          favorites.delete(name);
                        } else {
                          favorites.add(name);
                        }
                        localStorage.setItem(
                          "mapswap-favorites",
                          JSON.stringify(Array.from(favorites)),
                        );
                        renderResult(maps, xyz, url, min, currentTag);
                      },
                    },
                    t("⭐"),
                  )
                : t(""),
            ),
          ),
        ),
    ),
  );
};
