SHELL := /bin/bash

# local dev options
PORT := 8000

.PHONY: serve
serve: # Start dev server
	python3 -m http.server $(PORT)

.PHONY: format
format: # Format source code
	npx prettier --write **/*.js swap/maps.json **/*.html
